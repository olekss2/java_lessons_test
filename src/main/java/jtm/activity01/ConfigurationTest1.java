package jtm.activity01;

import org.junit.BeforeClass;

import jtm.testSuite.JTMTest;

public class ConfigurationTest1 extends ConfigurationTest {

	@BeforeClass
	public static void setUserAndPassword() {
		// It is not encouraged in class, but if you work at home,
		// you can change user and password for automated test
		// here:
		ConfigurationTest.user = "Arturs";
		ConfigurationTest.password = "0000";
		JTMTest.DefaultTimeout = 10; // Default timeout for unit tests
	}

}
